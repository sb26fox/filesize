﻿using System;
using FileSize;

namespace FileSizeConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var manager = new FileManager();
            var path = args[0];

            var size = manager.GetFileSize(path);

            Console.WriteLine(size);
            Console.ReadLine();
        }
    }
}