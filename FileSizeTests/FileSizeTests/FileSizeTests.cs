﻿using NUnit.Framework;
using Moq;
using FileSize;

namespace FileSizeTests
{
    [TestFixture]
    public class FileSizeTests
    {
        [Test]
        public void RetunrnTrue_IfFile_IsExists()
        {
            var stub = Mock.Of<IFileManager>(x => x.IsExists(@"D:\test.txt") == true);
            var mng = new StubFileManager(stub);

            bool isExists = mng.Manager.IsExists(@"D:\test.txt");

            Assert.IsTrue(isExists);
        }


        [Test]
        public void ReturnSize_IfFile_IsExists()
        {
            var stub = new Mock<IFileManager>();
            stub.Setup(x => x.IsExists(@"D:\test.txt")).Returns(true);
            stub.Setup(x => x.GetFileSize(@"D:\test.txt")).Returns(100);
            var mng = new StubFileManager(stub.Object);

            var fileSize = mng.Manager.GetFileSize(@"D:\test.txt");

            Assert.AreEqual(100, fileSize);
        }
    }
}