﻿using System.IO;

namespace FileSize
{
    public interface IFileManager
    {
        bool IsExists(string path);
        double GetFileSize(string path);
    }

    public class FileManager : IFileManager
    {
        public double GetFileSize(string path)
        {
            if (IsExists(path))
            {
                var file = new FileInfo(path);
                return file.Length;
            }

            return 0;
        }

        public bool IsExists(string path)
        {
            if (File.Exists(path))
            {
                return true;
            }

            return false;
        }
    }

    public class StubFileManager
    {
        public IFileManager Manager;

        public StubFileManager()
        {
            Manager = new FileManager();
        }

        public StubFileManager(IFileManager manager)
        {
            Manager = manager;
        }
    }
}